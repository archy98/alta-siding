$(function(){
	$('.siding-header-slider').slick({
  		dots: true,
		arrows: false,
  		infinite: true,
  		speed: 500,
  		slidesToScroll: 1,
  		slidesToShow: 1,
  		autoplay:true,
		autoplaySpeed:6000,

	});
	$('.siding-item-block-slider').on('init', function(event, slick){
		$('.siding-item-block-slider .slide .slide-img').css({
                	'background-size': 'cover'
		});
	});
	$('.siding-item-block-slider').slick({
  		dots: false,
		arrows: true,
  		infinite: false,
  		speed: 500,
  		slidesToScroll: 1,
  		slidesToShow: 6,
		initialSlide: 0,
		nextArrow: '<a class="btn slick-next"><div class="icon-arrow-r"></div></a>',
		prevArrow: '<a class="btn slick-prev"><div class="icon-arrow-l"></div></a>',
		responsive: [
    		{
      			breakpoint: 1600,
      			settings: {
  				slidesToShow: 4,

      			},
      			breakpoint: 1023,
      			settings: {
  				slidesToShow: 3,

      			}

    		}]
	});
	$('.siding-header-links a').click(function(){
       		var target = $(this).attr('href');
       		$('html, body').animate({scrollTop: $(target).offset().top}, 300);
       		return false; 
	}); 

	$('.siding-select-block .siding-select-current').on('click',function(){
		var block = $(this).parents('.siding-select-block').first();
		var list = block.find('.siding-select-list');
		list.toggle();
	});

	$('.siding-select-block .siding-select-list .siding-select-item').on('click',function(){
		var elem = $(this);
		var block = $(this).parents('.siding-select-block').first();
		var list = block.find('.siding-select-list');
		var current = block.find('.siding-select-current-txt');
 
                current.text(elem.text());
		list.toggle();
	});	
	$('#AltaBoard input[name^="collection"]').on('change',function(){
		var collection = [];		
		var type = [];		
		//��������� ������ ��������� ������� ����� �������
		$('#AltaBoard input[name^="collection"]:checked').each(function(k, v) { 
                	collection.push($(this).val());
		});
		//���� ������ ������, �� ���������� ��� �������� ������� ���
		if(collection.length==0)
		{
			$('#AltaBoard input[name^="type"]').each(function(k2, v2) { 
				var current_type = $(this);
				current_type.parents('.chbox-wrap').removeClass('hide').show();
			});
 			return false;
		}
		//���� ������ �� ������, �� ��������� ���������� ��� �������� ��������� ������� ���
		$('#AltaBoard input[name^="type"]').each(function(k2, v2) { 
			var type_list = (($(this).data('collection-id')).toString()).split('-');
			var current_type = $(this);

			var show_element = 0;
			$.each(type_list,function (key,value) {
                        	if($.inArray(value, collection)!=-1)
				{
                                	show_element = 1;
				}
                		type.push(value);
			});		
			if(!show_element) {
				current_type.parents('.chbox-wrap').addClass('hide').hide();
			}
			else {
				current_type.parents('.chbox-wrap').removeClass('hide').show();
			}
		});
	});
	$('#AltaBoard input[name^="collection"],#AltaBoard input[name^="type"]').on('change',function(){
		get_filter_res();
	});
});

function get_filter_res()
{
	$('#AltaBoard .siding-item-filter-res .s-res-color').each(function(k, v) {
		$(this).removeClass('hide').show();
	});
	var collection = [];		
	var type = [];		

	$('#AltaBoard input[name^="collection"]:checked').each(function(k, v) { 
                collection.push($(this).val());
	});
	$('#AltaBoard .chbox-wrap:not(.hide) input[name^="type"]:checked').each(function(k, v) { 
                type.push($(this).val());
	});
	//console.log('collection'+collection);
	//console.log('type'+type);

	$('#AltaBoard .siding-item-filter-res .s-res-color').each(function(k, v) {
		var current_elem = $(this);	
		if(collection.length>0) {

                        if($.inArray((current_elem.data('collection-id')).toString(), collection)==-1) {
				current_elem.addClass('hide').hide();
			}	        	
			else {
				current_elem.removeClass('hide').show();
			}
		}
		if(type.length>0) {
                        if($.inArray((current_elem.data('type-id')).toString(), type)==-1) {
				current_elem.not('.hide').addClass('hide').hide();
			}	        	
			else {
				current_elem.not('.hide').removeClass('hide').show();
			}
		}	
	});
}

