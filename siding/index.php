<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
<?$APPLICATION->SetAdditionalCSS("/siding/css/siding.css?t=".time());?>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
						</div><!--post_block-->		
					</div><!--type_content_r-->		
				</div><!--type_page_content-->
			</div><!--content-->
		</div><!--wrapper_i-->
	</div><!--wrapper-->
</div><!--layout-->

<div class="siding-page-header">
	<div class="siding-header-left">
		<div class="siding-header-slider">
			<div class="slide" style="background-image:url(/siding/images/alta_siding.jpg);"></div>
			<div class="slide" style="background-image:url(/siding/images/alta_board.jpg);"></div>
			<div class="slide" style="background-image:url(/siding/images/alaska.jpg);"></div>
			<div class="slide" style="background-image:url(/siding/images/canada.jpg);"></div>
			<div class="slide" style="background-image:url(/siding/images/blockhouse.jpg);"></div>
			<div class="slide" style="background-image:url(/siding/images/fasad_panel.jpg);"></div>
		</div>
	</div>
	<div class="siding-header-right">
		<div class="table">
			<div class="table-row">
				<div class="table-cell">
					<h1>Сайдинг &laquo;Альта-Профиль&raquo;</h1>
					<ul class="siding-header-links">
						<li class="siding-header-links-item"><a href="#AltaSiding">Альта-сайдинг</a> - экономичный вариант</li>
						<li class="siding-header-links-item"><a href="#AltaBoard">Альта-борд</a> - не отличим от сайдинга из натуральной доски</li>
						<li class="siding-header-links-item"><a href="#Alaska">Аляска</a> - выдерживает мороз до -65 °С</li>
						<li class="siding-header-links-item"><a href="#Canada">Канада плюс</a> - 18 уникальных цветов - повышенная цветостойкость</li>
						<li class="siding-header-links-item"><a href="#BlockHouse">Блок хаус</a> - стены дома будут похожи на бревенчатый сруб</li>
						<li class="siding-header-links-item"><a href="#Panel">Фасадные панели</a> - имитируют камень и кирпич</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="s-clear"></div>
</div>

<div id="layout2" class="layout"><!--layout-->
	<div class="wrapper"><!--wrapper-->
		<div class="wrapper_i"><!--wrapper_i-->
			<div class="content"><!--content-->
				<div class="type_page_content"><!--type_page_content-->
					<div class="type_content_r"><!--type_content_r-->		
						<div class="post_block"><!--post_block-->		
							<div class="siding-utp-block">
								<div class="siding-utp-list">
									<div class="siding-utp-item">
										<span class="siding-utp-item-image"></span>
										<span class="siding-utp-item-header">Приём заказов<br/>круглосуточно</span>
										<span class="siding-utp-item-text">Мы работаем 24 часа в сутки 7 дней в неделю 365 дней в году, поэтому оставить заказ Вы можете в любое удобное для Вас время.</span>
									</div>
									<div class="siding-utp-item">
										<span class="siding-utp-item-image"></span>
										<span class="siding-utp-item-header">Оплата при<br/>получении</span>
										<span class="siding-utp-item-text">Вы делаете заказ, при получении товара у диллера в вашем регионе возможна оплата наличными, банковской картой или безналичным переводом</span>
									</div>
									<div class="siding-utp-item">
										<span class="siding-utp-item-image"></span>
										<span class="siding-utp-item-header">Быстрая<br/>доставка</span>
										<span class="siding-utp-item-text">Выполним быструю и бережную доставку выбранных материалов (условия доставки необходимо уточнять у диллера в вашем регионе)</span>
									</div>
									<div class="siding-utp-item">
										<span class="siding-utp-item-image"></span>
										<span class="siding-utp-item-header">Проконсультируем<br/>по телефону</span>
										<span class="siding-utp-item-text">Для Вас работает бесплатный номер 8800. Наш оператор выслушает Вас и простым понятным, языком вежливо ответит на все ваши вопросы.</span>
									</div>
								</div>
							</div>

<?if(1){?>
						</div><!--post_block-->		
					</div><!--type_content_r-->		
				</div><!--type_page_content-->
			</div><!--content-->
		</div><!--wrapper_i-->
	</div><!--wrapper-->
</div><!--layout-->
<?}?>
<?if(1){?>
<div id="layout5" class="layout"><!--layout-->
	<div class="wrapper"><!--wrapper-->
		<div class="wrapper_i"><!--wrapper_i-->
			<div class="content"><!--content-->
				<div class="type_page_content"><!--type_page_content-->
					<div class="type_content_r"><!--type_content_r-->		
						<div class="post_block"><!--post_block-->

						</div><!--post_block-->		
					</div><!--type_content_r-->		
				</div><!--type_page_content-->
			</div><!--content-->
		</div><!--wrapper_i-->
	</div><!--wrapper-->
</div><!--layout-->
<?}?>
<?if(1){?>
<div id="layout6" class="layout"><!--layout-->
	<div class="wrapper"><!--wrapper-->
		<div class="wrapper_i"><!--wrapper_i-->
			<div class="content"><!--content-->
				<div class="type_page_content"><!--type_page_content-->
					<div class="type_content_r"><!--type_content_r-->		
						<div class="post_block"><!--post_block-->
<?}?>		


							<div class="siding-item-block siding-alta" id="AltaSiding">
								<div class="siding-left">
									<h2 class="siding-h2-title"><span>Альта - сайдинг</span></h2>
                                                                        <div class="s-clear"></div>
									<div class="siding-inner-left">
			                                                	<p>
			                                                		Классический виниловый сайдинг, изгтовленный из прочного и долговечного полимерного материала. Разработан в России - для нашего климата и наших домов. Серия &laquo;Альта - сайдинг&raquo; производится с 1999 года. Тысячи владельцев домов проверили её качество и способность противостоять &laquo;сюрпризам&raquo; российского климата.
										</p>
										<div class="siding-desc tablet-view">
											Лучшее решение для<br/>небольшого бюджета
										</div>
                                                                                <div class="pc-view">
											<div class="siding-item-filter">
												<div class="siding-item-filter-header">
													Тип сайдинга:	
												</div>
												<div class="siding-item-filter-input check_face">
													<form>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="type[alta_siding][1]" id="sfilter1" value="1">
																<span class="chbox-bg"></span>
																<label for="sfilter1">Виниловый</label>
															</span>
														</div>
														<div class="s-clear"></div>
													</form>	
												</div>
											</div>	
											<div class="s-clear"></div>
											<div class="siding-item-filter-res">	
												<span class="s-res-color" style="background-color:#efefef;"></span>
												<span class="s-res-color" style="background-color:#c5d1bd;"></span>
												<span class="s-res-color" style="background-color:#55230c;"></span>
												<span class="s-res-color" style="background-color:#eae3d1;"></span>
												<span class="s-res-color" style="background-color:#e3d9cd;"></span>
												<span class="s-res-color" style="background-color:#fce6d1;"></span>
												<span class="s-res-color" style="background-color:#678289;"></span>
												<span class="s-res-color" style="background-color:#dac6ae;"></span>
												<span class="s-res-color" style="background-color:#c7cdcd;"></span>
												<span class="s-res-color" style="background-color:#c7dbc2;"></span>
												<span class="s-res-color" style="background-color:#2b4c51;"></span>
												<span class="s-res-color" style="background-color:#f7f2ba;"></span>
												<span class="s-res-color" style="background-color:#532901;"></span>
											</div>											
										</div>
									</div>
									<div class="siding-inner-right pc-view">
										<div class="siding-desc">
											Лучшее решение для<br/>небольшого бюджета
										</div>
										<ul class="dots_list">
											<li><p>Доступная цена</p></li>
											<li><p>10 цветов</p></li>
											<li><p>Не поддерживает горение</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Морозостойкость до -60 &deg;C</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>
								<div class="siding-right">									               	
									<!--<img src="/siding/images/alta_siding.jpg">-->
									<img class="siding-element" src="/siding/images/alta_siding_element.png">
								</div>
								<div class="s-clear"></div>

                                                                <div class="tablet-view siding-tablet-bottom-block">
                                                                       	<div class="siding-tablet-filter-block">
										<div class="siding-item-filter">
											<div class="siding-item-filter-header">
												Тип сайдинга:	
											</div>
											<div class="siding-item-filter-input check_face">
												<form>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="type[alta_siding][1]" id="sfilter1" value="1">
															<span class="chbox-bg"></span>
															<label for="sfilter1">Виниловый</label>
														</span>
													</div>
													<div class="s-clear"></div>
												</form>	
											</div>
										</div>	
										<div class="s-clear"></div>
										<div class="siding-item-filter-res">	
											<span class="s-res-color" style="background-color:#efefef;"></span>
											<span class="s-res-color" style="background-color:#c5d1bd;"></span>
											<span class="s-res-color" style="background-color:#55230c;"></span>
											<span class="s-res-color" style="background-color:#eae3d1;"></span>
											<span class="s-res-color" style="background-color:#e3d9cd;"></span>
											<span class="s-res-color" style="background-color:#fce6d1;"></span>
											<span class="s-res-color" style="background-color:#678289;"></span>
											<span class="s-res-color" style="background-color:#dac6ae;"></span>
											<span class="s-res-color" style="background-color:#c7cdcd;"></span>
											<span class="s-res-color" style="background-color:#c7dbc2;"></span>
											<span class="s-res-color" style="background-color:#2b4c51;"></span>
											<span class="s-res-color" style="background-color:#f7f2ba;"></span>
											<span class="s-res-color" style="background-color:#532901;"></span>
										</div>											
									</div>
                                                                       	<div class="siding-tablet-list-block">
										<ul class="dots_list">
											<li><p>Доступная цена</p></li>
											<li><p>10 цветов</p></li>
											<li><p>Не поддерживает горение</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Морозостойкость до -60 &deg;C</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>

								<div class="siding-item-block-slider">
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
								</div>
								<div class="siding-btn-block">
									<a href="/catalog/promo/alta-saiding/" class="btn_red">Подробнее</a>
								</div>
							</div>
							<div class="siding-item-block alta-board" id="AltaBoard">
								<div class="siding-left">
									<h2 class="siding-h2-title"><span>Альта борд</span></h2>
                                                                        <div class="s-clear"></div>
									<div class="siding-inner-left">
			                                                	<p>
											Воссоздает классическую форму традиционного сайдинга с использованием современных материалов и технологий. Практически не отличим от натуральной доски.
											&laquo;Альта-Борд&raquo; выбирают ценители классики, для которых важны высокие эксплуатационные характеристики и красивый натурральный внешний вид.	
										</p>
										<div class="siding-desc tablet-view">
											Прочный сайдинг<br/>с классической формой<br/>&laquo;ёлочка&raquo;
										</div>
                                        	                                <div class="pc-view">
											<div class="siding-item-filter">
												<div class="siding-item-filter-header">
													Коллекции:	
												</div>
												<div class="siding-item-filter-input check_face">
													<form>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="collection[alta_board][1]" id="sfilter2" value="1">
																<span class="chbox-bg"></span>
																<label for="sfilter2">Стандарт</label>
															</span>
														</div>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="collection[alta_board][2]" id="sfilter3" value="2">
																<span class="chbox-bg"></span>
																<label for="sfilter3">Тимбер</label>
															</span>
														</div>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="collection[alta_board][3]" id="sfilter4" value="3">
																<span class="chbox-bg"></span>
																<label for="sfilter4">Тимбер Pro</label>
															</span>
														</div>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="collection[alta_board][4]" id="sfilter5" value="4">
																<span class="chbox-bg"></span>
																<label for="sfilter5">Элит</label>
															</span>
														</div>
													</form>	
												</div>
											</div>	
											<div class="siding-item-filter">
												<div class="siding-item-filter-header">
													Тип сайдинга:	
												</div>
												<div class="siding-item-filter-input check_face">
													<form>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="type[alta_board][1]" id="sfilter6" value="1" data-collection-id="1">
																<span class="chbox-bg"></span>
																<label for="sfilter6">Виниловый</label>
															</span>
														</div>
													        <div class="siding-filter-param">
															<span class="chbox-wrap">
																<input type="checkbox" name="type[alta_board][2]" id="sfilter7" value="2" data-collection-id="1-2-3-4">
																<span class="chbox-bg"></span>
																<label for="sfilter7">Акриловый</label>
															</span>
														</div>
													</form>	
												</div>
											</div>
											<div class="s-clear"></div>
											<div class="siding-item-filter-res">	
												<span class="s-res-color" style="background-color:#4f8164;" data-collection-id="1" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#8e6d5e;" data-collection-id="2" data-type-id="2"></span>
												<span class="s-res-color" style="background-color:#976266;" data-collection-id="2" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#beb179;" data-collection-id="3" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#7f7572;" data-collection-id="1" data-type-id="2"></span>
												<span class="s-res-color" style="background-color:#e3e3e3;" data-collection-id="1" data-type-id="2"></span>
												<span class="s-res-color" style="background-color:#d4ba99;" data-collection-id="4" data-type-id="2"></span>
												<span class="s-res-color" style="background-color:#e7d5af;" data-collection-id="2" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#f9d486;" data-collection-id="2" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#ddaa77;" data-collection-id="4" data-type-id="2"></span>
												<span class="s-res-color" style="background-color:#dcd9cb;" data-collection-id="4" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#80655a;" data-collection-id="4" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#d8d6c9;" data-collection-id="2" data-type-id="1"></span>
												<span class="s-res-color" style="background-color:#e8dec1;" data-collection-id="3" data-type-id="2"></span>
											</div>	
										</div>											
									</div>
									<div class="siding-inner-right pc-view">
										<div class="siding-desc">
											Прочный сайдинг<br/>с классической формой<br/>&laquo;ёлочка&raquo;
										</div>
										<ul class="dots_list">
											<li><p>Выглядит, как настоящая древесина</p></li>
											<li><p>Шумоизоляция</p></li>
											<li><p>14 цветов</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Горение не поддерживает</p></li>
											<li><p>Простота монтажа</p></li>
											<li><p>Эксклюзивный внешний вид</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>
								<div class="siding-right">									               	
									<!--<img src="/siding/images/alta_siding.jpg">-->
									<img class="siding-element" src="/siding/images/alta_board_element.png">
								</div>
								<div class="s-clear"></div>

                                                                <div class="tablet-view siding-tablet-bottom-block">
                                                                       	<div class="siding-tablet-filter-block">
										<div class="siding-item-filter">
											<div class="siding-item-filter-header">
											Коллекции:	
											</div>
											<div class="siding-item-filter-input check_face">
												<form>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="collection[alta_board][1]" id="sfilter2" value="1">
															<span class="chbox-bg"></span>
															<label for="sfilter2">Стандарт</label>
														</span>
													</div>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="collection[alta_board][2]" id="sfilter3" value="2">
															<span class="chbox-bg"></span>
															<label for="sfilter3">Тимбер</label>
														</span>
													</div>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="collection[alta_board][3]" id="sfilter4" value="3">
															<span class="chbox-bg"></span>
															<label for="sfilter4">Тимбер Pro</label>
														</span>
													</div>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="collection[alta_board][4]" id="sfilter5" value="4">
															<span class="chbox-bg"></span>
															<label for="sfilter5">Элит</label>
														</span>
													</div>
												</form>	
											</div>
										</div>	
										<div class="siding-item-filter">
											<div class="siding-item-filter-header">
												Тип сайдинга:	
											</div>
											<div class="siding-item-filter-input check_face">
												<form>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="type[alta_board][1]" id="sfilter6" value="1" data-collection-id="1">
															<span class="chbox-bg"></span>
															<label for="sfilter6">Виниловый</label>
														</span>
													</div>
												        <div class="siding-filter-param">
														<span class="chbox-wrap">
															<input type="checkbox" name="type[alta_board][2]" id="sfilter7" value="2" data-collection-id="1-2-3-4">
															<span class="chbox-bg"></span>
															<label for="sfilter7">Акриловый</label>
														</span>
													</div>
												</form>	
											</div>
										</div>
										<div class="s-clear"></div>
										<div class="siding-item-filter-res">	
											<span class="s-res-color" style="background-color:#4f8164;" data-collection-id="1" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#8e6d5e;" data-collection-id="2" data-type-id="2"></span>
											<span class="s-res-color" style="background-color:#976266;" data-collection-id="2" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#beb179;" data-collection-id="3" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#7f7572;" data-collection-id="1" data-type-id="2"></span>
											<span class="s-res-color" style="background-color:#e3e3e3;" data-collection-id="1" data-type-id="2"></span>
											<span class="s-res-color" style="background-color:#d4ba99;" data-collection-id="4" data-type-id="2"></span>
											<span class="s-res-color" style="background-color:#e7d5af;" data-collection-id="2" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#f9d486;" data-collection-id="2" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#ddaa77;" data-collection-id="4" data-type-id="2"></span>
											<span class="s-res-color" style="background-color:#dcd9cb;" data-collection-id="4" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#80655a;" data-collection-id="4" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#d8d6c9;" data-collection-id="2" data-type-id="1"></span>
											<span class="s-res-color" style="background-color:#e8dec1;" data-collection-id="3" data-type-id="2"></span>
										</div>	
									</div>
                                                                       	<div class="siding-tablet-list-block">
										<ul class="dots_list">
											<li><p>Выглядит, как настоящая древесина</p></li>
											<li><p>Шумоизоляция</p></li>
											<li><p>14 цветов</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Горение не поддерживает</p></li>
											<li><p>Простота монтажа</p></li>
											<li><p>Эксклюзивный внешний вид</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>

									</div>
									<div class="s-clear"></div>
								</div>


								<div class="siding-item-block-slider">
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
								</div>
								<div class="siding-btn-block">
									<a href="/catalog/promo/alta-board/" class="btn_red">Подробнее</a>
								</div>
							</div>
							<div class="siding-item-block alaska" id="Alaska">
								<div class="siding-left">
									<h2 class="siding-h2-title"><span>Аляска</span></h2>
                                                                        <div class="s-clear"></div>
									<div class="siding-inner-left">
			                                                <p>
			                                                	Сайдинг выдерживает сильный мороз до -65 &deg;C. Это возможно благодаря оптимальному размеру панелей 3000 на 205 мм (при резком перепаде температур геометрия панели не нарушается), а также усовершенствованному нижнему слою панели.
										Панели надежно служат без визуальных и функциональный изменений даже в Якутии, где абсолютный температурный минимум достигает -64,4 &deg;C.
									</p>
									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Коллекции:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[alaska][1]" id="sfilter8" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter8">Классик</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[alaska][2]" id="sfilter9" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter9">Люкс</label>
													</span>
												</div>
											</form>	
										</div>
									</div>	

									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Тип сайдинга:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[alta_board][1]" id="sfilter10" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter10">Виниловый</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[alta_board][2]" id="sfilter11" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter11">Акриловый</label>
													</span>
												</div>
											</form>	
										</div>
									</div>	
									<div class="s-clear"></div>
									<div class="siding-item-filter-res">	
										<span class="s-res-color" style="background-color:#c9bdad;"></span>
										<span class="s-res-color" style="background-color:#c5d6e8;"></span>
										<span class="s-res-color" style="background-color:#a57970;"></span>
										<span class="s-res-color" style="background-color:#9a8292;"></span>
										<span class="s-res-color" style="background-color:#ebcea2;"></span>
										<span class="s-res-color" style="background-color:#7f8a7c;"></span>
										<span class="s-res-color" style="background-color:#c28493;"></span>
										<span class="s-res-color" style="background-color:#ebe8e3;"></span>
										<span class="s-res-color" style="background-color:#cdc4b3;"></span>
										<span class="s-res-color" style="background-color:#d3dccb;"></span>
										<span class="s-res-color" style="background-color:#eadbc4;"></span>
									</div>											
									</div>
									<div class="siding-inner-right">
										<div class="siding-desc">
											Морозостойкий сайдинг<br/>для холодных зим
										</div>
										<ul class="dots_list">
											<li><p>Морозостойкость до -65 &deg;C</p></li>
											<li><p>Выдерживает резкие перепады температуры</p></li>
											<li><p>Углубленное тиснение, для точной имитации дерева</p></li>
											<li><p>11 цветов</p></li>
											<li><p>Ударопрочный</p></li>
											<li><p>Суперцветостойкая коллекция Люкс</p></li>
											<li><p>Не поддерживает горение</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>
								<div class="siding-right">									               	
									<!--<img src="/siding/images/alta_siding.jpg">-->
									<img class="siding-element" src="/siding/images/alaska_element.png">
								</div>
								<div class="s-clear"></div>
								<div class="siding-item-block-slider">
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
								</div>
								<div class="siding-btn-block">
									<a href="/catalog/promo/sayding-alaska/" class="btn_red">Подробнее</a>
								</div>
							</div>
							<div class="siding-item-block canada" id="Canada">
								<div class="siding-left">
									<h2 class="siding-h2-title"><span>Канада Плюс</span></h2>
                                                                        <div class="s-clear"></div>
									<div class="siding-inner-left">
			                                                <p>
										Виниловый сайндинг с улучшенными эксплуатационными свойствами устойчив к морозам. Панели сохраняют ударопрочность при температуре -60 &deg;C, поэтому их можно монтировать даже зимой. Линейка включает 18 цветов.	
									</p>
									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Коллекции:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[canada][1]" id="sfilter12" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter12">Престиж</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[canada][2]" id="sfilter13" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter13">Премиум</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[canada][3]" id="sfilter14" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter14">Карелия</label>
													</span>
												</div>
											</form>	
										</div>
									</div>	

									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Тип сайдинга:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[canada][1]" id="sfilter15" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter15">Виниловый</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[canada][2]" id="sfilter16" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter16">Акриловый</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[canada][3]" id="sfilter17" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter17">Крашеный</label>
													</span>
												</div>

											</form>	
										</div>
									</div>	
									<div class="s-clear"></div>
									<div class="siding-item-filter-res">	
										<span class="s-res-color" style="background-color:#5d8ac1;"></span>
										<span class="s-res-color" style="background-color:#b05755;"></span>
										<span class="s-res-color" style="background-color:#764929;"></span>
										<span class="s-res-color" style="background-color:#639277;"></span>
										<span class="s-res-color" style="background-color:#bab797;"></span>
										<span class="s-res-color" style="background-color:#be764a;"></span>
										<span class="s-res-color" style="background-color:#817980;"></span>
										<span class="s-res-color" style="background-color:#cfc96e;"></span>
										<span class="s-res-color" style="background-color:#99676d;"></span>
										<span class="s-res-color" style="background-color:#8d3c3c;"></span>
										<span class="s-res-color" style="background-color:#9e7269;"></span>
										<span class="s-res-color" style="background-color:#bababa;"></span>
										<span class="s-res-color" style="background-color:#e8c685;"></span>
										<span class="s-res-color" style="background-color:#dfceca;"></span>
										<span class="s-res-color" style="background-color:#fbf1be;"></span>
										<span class="s-res-color" style="background-color:#b2d1b2;"></span>
										<span class="s-res-color" style="background-color:#f6d0c4;"></span>
										<span class="s-res-color" style="background-color:#fae6a4;"></span>
									</div>											
									</div>
									<div class="siding-inner-right">
										<div class="siding-desc">
											Насыщенные цвета для<br/>дизайн-решений
										</div>
										<ul class="dots_list">
											<li><p>Уникальные насыщенные цвета</p></li>
											<li><p>Морозостойкость до -60 &deg;C</p></li>
											<li><p>18 цветов</p></li>
											<li><p>Панели не "ведёт" даже при сильном нагревании</p></li>
											<li><p>Не горит</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>
								<div class="siding-right">									               	
									<!--<img src="/siding/images/alta_siding.jpg">-->
									<img class="siding-element" src="/siding/images/canada_element.png">
								</div>
								<div class="s-clear"></div>
								<div class="siding-item-block-slider">
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
								</div>
								<div class="siding-btn-block">
									<a href="/catalog/promo/kanada-plus/" class="btn_red">Подробнее</a>
								</div>
							</div>
							<div class="siding-item-block blockhouse" id="BlockHouse">
								<div class="siding-left">
									<h2 class="siding-h2-title"><span>Блокхаус</span></h2>
                                                                        <div class="s-clear"></div>
									<div class="siding-inner-left">
			                                                <p>
			                                                	Дом с сайдингом &laquo;Блок-хаус&raquo; похож на сруб - традиционный и уютный. На вид его сложно отличить от натуральной древесины, зато он практичнее, не боится влаги и морозов. Виниловый сайдинг &laquo;Блок-хаус&raquo; морозоустойчив и сохраняет ударопрочность при температура до -60 &deg;C. Поэтому он идеально подойдет для северных регионов страны, где традиционно любят дома из сруба.
									</p>
									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Коллекции:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[blockhouse][1]" id="sfilter18" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter18">Однопереломный</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[blockhouse][2]" id="sfilter19" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter19">Двухпереломный</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="collection[blockhouse][3]" id="sfilter20" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter20">Двухпереломный малый</label>
													</span>
												</div>
											</form>	
										</div>
									</div>	

									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Тип сайдинга:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[blockhouse][1]" id="sfilter21" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter21">Виниловый</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[blockhouse][2]" id="sfilter22" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter22">Акриловый</label>
													</span>
												</div>
											        <div class="siding-filter-param">
													<span class="chbox-wrap">
														<input type="checkbox" name="type[blockhouse][3]" id="sfilter23" value="1">
														<span class="chbox-bg"></span>
														<label for="sfilter23">Крашеный</label>
													</span>
												</div>

											</form>	
										</div>
									</div>	
									<div class="s-clear"></div>
									<div class="siding-item-filter-res">	
										<span class="s-res-color" style="background-color:#9e5f2d;"></span>
										<span class="s-res-color" style="background-color:#835534;"></span>
										<span class="s-res-color" style="background-color:#9f6c67;"></span>
										<span class="s-res-color" style="background-color:#ddd0be;"></span>
										<span class="s-res-color" style="background-color:#efc98e;"></span>
										<span class="s-res-color" style="background-color:#e4cfcb;"></span>
									</div>											
									</div>
									<div class="siding-inner-right">
										<div class="siding-desc">
											Надежная альтернатива<br/>отделке деревом
										</div>
										<ul class="dots_list">
											<li><p>Похож на настоящие бревна</p></li>
											<li><p>Морозостойкость до -60 &deg;C</p></li>
											<li><p>7 цветов</p></li>
											<li><p>Ударопрочный</p></li>
											<li><p>Не поддерживает горение</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>
								<div class="siding-right">									               	
									<!--<img src="/siding/images/alta_siding.jpg">-->
									<img class="siding-element" src="/siding/images/blockhouse_element.png">
								</div>
								<div class="s-clear"></div>
								<div class="siding-item-block-slider">
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
								</div>
								<div class="siding-btn-block">
									<a href="/catalog/promo/blockhouse/" class="btn_red">Подробнее</a>
								</div>
							</div>
							<div class="siding-item-block panel" id="Panel">
								<div class="siding-left">
									<h2 class="siding-h2-title"><span>Фасадные панели</span></h2>
                                                                        <div class="s-clear"></div>
									<div class="siding-inner-left">
			                                                <p>
			                                                	Цоколь вашего дома выглядит солидно и элегантно. Надежно защищен от ветра, влаги, снега, подмывания и не выгорает на солнце. Фасад, облицованный цокольным сайдингом &laquo;Альта-Профиль&raquo;, не отличить от натурального камня или кирпича даже с расстояния трех шагов. Выберите свой дизайн из 13 коллекций!
									</p>
									<div class="siding-item-filter">
										<div class="siding-item-filter-header">
											Коллекции:	
										</div>
										<div class="siding-item-filter-input check_face">
											<form>
											        <div class="siding-filter-param">
											        	<div class="siding-select-block">
											        	 	<span class="siding-select-current"><span class="siding-select-current-txt">Все коллекции</span></span>
													        <div class="siding-select-list">
											        	 		<span class="siding-select-item">Все коллекции</span>
											        	 		<span class="siding-select-item">Престиж</span>
											        	 		<span class="siding-select-item">Премиум</span>
											        	 		<span class="siding-select-item">Карелия</span>
														</div>
													</div>
												</div>
											</form>	
										</div>
									</div>	

									<div class="s-clear"></div>
									<div class="siding-item-filter-res">	
										<span class="s-res-color" style="background-image:url(/siding/images/panel_1.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_2.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_3.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_4.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_5.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_6.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_7.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_8.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_9.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_10.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_11.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_12.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_13.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_14.jpg);"></span>
										<span class="s-res-color" style="background-image:url(/siding/images/panel_15.jpg);"></span>
									</div>											
									</div>
									<div class="siding-inner-right">
										<div class="siding-desc">
											Имитация<br/>натуральных материалов
										</div>
										<ul class="dots_list">
											<li><p>Имитируют настоящий камень и кирпич</p></li>
											<li><p>Толще, чем большинство панелей на рынке</p></li>
											<li><p>Морозостойкость до -60 &deg;C</p></li>
											<li><p>80 моделей</p></li>
											<li><p>Стойкость цвета</p></li>
											<li><p>Ударопрочный</p></li>
											<li><p>Размеры панели <b>3660 x 230 x 1,1 мм</b></p></li>
										</ul>
									</div>
									<div class="s-clear"></div>
								</div>
								<div class="siding-right">									               	
									<!--<img src="/siding/images/alta_siding.jpg">-->
									<img class="siding-element" src="/siding/images/panel_element.png">
								</div>
								<div class="s-clear"></div>
								<div class="siding-item-block-slider">
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>
										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_03.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_03.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_04.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_04.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_01.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_01.jpg);"></span>

										</a>
									</div>	
									<div class="slide">
										<a href="/siding/images/s1_photo_02.jpg" rel="gallery1" class="fancy-popup-gallery">
											<span class="slide-img" style="background-image:url(/siding/images/s1_photo_02.jpg);"></span>

										</a>
									</div>	
								</div>
								<div class="siding-btn-block">
									<a href="/catalog/promo/fasadnie-paneli/" class="btn_red">Подробнее</a>
								</div>
							</div>
							<div class="siding-table-container">
								<h2 class="siding-h2-title"><span>Различия фасадных материалов</span></h2>
								<div class="siding-table">
									<div class="siding-table-row">
										<div class="siding-table-cell">
	                                                                        	Параметр
										</div>						
										<div class="siding-table-cell">
	                                                                        	Альта-сайдинг
										</div>						
										<div class="siding-table-cell">
	                                                                                Альта-борд
										</div>						
										<div class="siding-table-cell">
	                                                                                Аляска
										</div>						
										<div class="siding-table-cell">
	                                                                                Канада Плюс
										</div>						
										<div class="siding-table-cell">
	                                                                                Блок хаус
										</div>						
										<div class="siding-table-cell">
	                                                                                Фасадные панели
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
											Подходит для фасада	
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
										<div class="siding-table-cell">
                                                                                        да
										</div>						
										<div class="siding-table-cell">
                                                                                        да
										</div>						
										<div class="siding-table-cell">
                                                                                        да
										</div>						
										<div class="siding-table-cell">
                                                                                        да
										</div>						
										<div class="siding-table-cell">
                                                                                        да
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										        Подходит для цоколя
										</div>						
										<div class="siding-table-cell">
										        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                        да
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										        Улучшенная имитация натуральных материалов (доска, бревно, камень, кирпич)
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										        Тепло и шумоизоляция
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
										        да
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                        <span class="s-no">нет</span>
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										       Повышенная морозостойкость
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										       Повышенная ударопрочность
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										       Пожаростойкость
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										       Цветостойкость
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
									</div>
									<div class="siding-table-row">
										<div class="siding-table-cell">
										       Упрощенный монтаж
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
										       да
										</div>						
										<div class="siding-table-cell">
                                                                                       <span class="s-no">нет</span>
										</div>						
									</div>
								</div>
							</div>
						</div><!--post_block-->		
					</div><!--type_content_r-->		
				</div><!--type_page_content-->
			</div><!--content-->
		</div><!--wrapper_i-->
	</div><!--wrapper-->
</div><!--layout-->
<div class="siding-choose-container">
	<div id="layout3" class="layout"><!--layout-->
		<div class="wrapper"><!--wrapper-->
			<div class="wrapper_i"><!--wrapper_i-->
				<div class="content"><!--content-->
					<div class="type_page_content"><!--type_page_content-->
						<div class="type_content_r"><!--type_content_r-->		
							<div class="post_block"><!--post_block-->		
								<div class="siding-choose-block">
									<div class="siding-choose-row">
										<div class="siding-choose-left">
											<h2 class="siding-h2-title"><span>Какой сайдинг выбрать?</span></h2>
											<p>
												В последнее время виниловый сайдинг в нашей стране стал по-настоящему народным продуктов, позволяющим в коротки сроки и за разумные деньги преобразить свой дом, офис и прочее, сделать из него &laquo;конфетку&raquo; на радость себе и зависть соседям. В такой ситуации очень актуальным становится вопрос, какой же все-таки выбирать сайдинг - импортный или отечественный, какой продукт максимально удовлетворяет параметрам &laquo;цена-качество&raquo;?
											</p>	
											<a href="/client-center/articles/oblitsovka-fasada/kak-vybrat-saiding/" class="btn_red">Узнать о выборе сайдинга</a>	
										</div>
										<div class="siding-choose-right">
										        <div class="siding-choose-video">
												<a href="https://www.youtube.com/watch?v=iY2gUdeiwTI">
													<img src="/siding/images/what_choose_img.jpg"/>
													<span class="siding-choose-video-icon"></span>				
												</a>
											</div>
											<a class="siding-choose-mobile-video" href="https://www.youtube.com/watch?v=iY2gUdeiwTI">
												<span class="siding-choose-video-icon"></span>				
											</a>
										</div>
									</div>
                                                                        <div class="s-clear"></div>
								</div>
							</div><!--post_block-->		
						</div><!--type_content_r-->		
					</div><!--type_page_content-->
				</div><!--content-->
			</div><!--wrapper_i-->
		</div><!--wrapper-->
	</div><!--layout-->
</div>
<div id="layout4" class="layout"><!--layout-->
	<div class="wrapper"><!--wrapper-->
		<div class="wrapper_i"><!--wrapper_i-->
			<div class="content"><!--content-->
				<div class="type_page_content"><!--type_page_content-->
					<div class="type_content_r"><!--type_content_r-->		
						<div class="post_block"><!--post_block-->		
							<div class="siding-useful">
								<h2 class="siding-h2-title"><span>Вам также может пригодиться</span></h2>
								<div class="siding-useful-block">
									<div class="siding-useful-list">
										<div class="siding-useful-item">
										        <div class="siding-useful-container">
												<span class="siding-useful-item-image"></span>
												<span class="siding-useful-item-text"><a href="/catalog/">Сервис по подбору<br/>сайдинга</a></span>
											</div>
										</div>
										<div class="siding-useful-item">
										        <div class="siding-useful-container">
												<span class="siding-useful-item-image"></span>
												<span class="siding-useful-item-text"><a href="/modulator/">Сервис по подбору цвета<br/>сайдинга</a></span>
											</div>
										</div>
										<div class="siding-useful-item">
										        <div class="siding-useful-container">
												<span class="siding-useful-item-image"></span>
												<span class="siding-useful-item-text"><a href="/calculator/">Сервис для расчета<br/>количества и стоимости<br/>сайдинга</a></span>
											</div>
										</div>
										<div class="siding-useful-item">
										        <div class="siding-useful-container">
												<span class="siding-useful-item-image"></span>
												<span class="siding-useful-item-text"><a href="/wherebuy/">Адрес точек продаж</a></span>
											</div>
										</div>
									</div>
								</div>
							</div>	
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>